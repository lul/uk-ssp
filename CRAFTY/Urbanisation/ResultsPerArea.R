#create urban/rural class

#Clear the memory
rm(list=ls())

# packages
library(maps)
library(mapdata)
library(maptools)
library(rgdal)
library(ggmap)
library(ggplot2)
library(rgeos)
library(broom)
library(plyr)
library(tidyverse)
library(raster)
library(here)

# set wd
ukssp_dir<-here::here() # finds the fhe directory where the .rproj file is located
source(file.path(ukssp_dir, 'EnvironSetup.R'))
remove(capitals_dir,furthermetrics_dir,population_dir)

wd<-file.path(urbanisation_dir,"Results")
setwd(wd)


#### Import Data ###############################################################

# function (for NUTS shapefiles) to create a file of urban extent per ssp
whatsurban.N<-function(shp,plotspath){ # function of a shapefile with area boundaries
  # 1. Step: Create data frame for our new data
  urb_table<-cbind(
    as.data.frame(shp),
    as.data.frame(matrix(data=NA,nrow=length(as.data.frame(shp)[,1]),ncol=45))
  )
  years<-c("2020","2030","2040","2050","2060","2070","2080","2090","2100")
  mapnames<-c(paste("SSP1",years,sep="."),
           paste("SSP2",years,sep="."),
           paste("SSP3",years,sep="."),
           paste("SSP4",years,sep="."),
           paste("SSP5",years,sep="."))
  colnames(urb_table)<-c(colnames(as.data.frame(shp)),mapnames)
  # 2. Step: Create empty raster stack
  r<-raster(paste(plotpath,"\\","SSP1.2020",".tif",sep=""))
  r[]<-0
  s<-list()
  for(i in 1:45){
    s[[i]]<-r
  }
  s<-stack(s)
  names(s)<-mapnames
  # 3. Step: Import all surface maps at 1km resolution to the stack
  for(i in 1:45){
    s[[i]]<-raster(paste(plotpath,"\\",mapnames[i],".tif",sep=""))
  }
  # 4. Step: Transform the area boundaries shapefile into a compatible raster
  shpT<-spTransform(shp,crs(r)) # adjust crs and extent
  area_r<-rasterize(shpT,r,"objectid")
  # 5. Step: Fill in our table
  startcol<-which(colnames(urb_table)==mapnames[1])
  endcol  <-which(colnames(urb_table)==mapnames[45])
  for (j in startcol:endcol){
    for (i in 1:nrow(urb_table)){
      urb_table[i,j]<-
        length(which(s[[j-startcol+1]][area_r==i]==1))/ # divide urban pixels in every area
        length(which(!is.na(s[[j-startcol+1]][area_r==i]))) # by total pixels in every area (excluding the sea, which is NA)
    }
  }
  return(urb_table)
}


# function (for LAD shapefile- has different ID colname) to create a file of urban extent per ssp
whatsurban.LAD<-function(shp,plotspath){ # function of a shapefile with area boundaries
  # 1. Step: Create data frame for our new data
  urb_table<-cbind(
    as.data.frame(shp),
    as.data.frame(matrix(data=NA,nrow=length(as.data.frame(shp)[,1]),ncol=45))
  )
  years<-c("2020","2030","2040","2050","2060","2070","2080","2090","2100")
  mapnames<-c(paste("SSP1",years,sep="."),
              paste("SSP2",years,sep="."),
              paste("SSP3",years,sep="."),
              paste("SSP4",years,sep="."),
              paste("SSP5",years,sep="."))
  colnames(urb_table)<-c(colnames(as.data.frame(shp)),mapnames)
  # 2. Step: Create empty raster stack
  r<-raster(paste(plotpath,"\\","SSP1.2020",".tif",sep=""))
  r[]<-0
  s<-list()
  for(i in 1:45){
    s[[i]]<-r
  }
  s<-stack(s)
  names(s)<-mapnames
  # 3. Step: Import all surface maps at 1km resolution to the stack
  for(i in 1:45){
    s[[i]]<-raster(paste(plotpath,"\\",mapnames[i],".tif",sep=""))
  }
  # 4. Step: Transform the area boundaries shapefile into a compatible raster
  shpT<-spTransform(shp,crs(r)) # adjust crs and extent
  area_r<-rasterize(shpT,r,"OBJECTID")
  # 5. Step: Fill in our table
  startcol<-which(colnames(urb_table)==mapnames[1])
  endcol  <-which(colnames(urb_table)==mapnames[45])
  for (j in startcol:endcol){
    for (i in 1:nrow(urb_table)){
      urb_table[i,j]<-
        length(which(s[[j-startcol+1]][area_r==i]==1))/ # divide urban pixels in every area
        length(which(!is.na(s[[j-startcol+1]][area_r==i]))) # by total pixels in every area (excluding the sea, which is NA)
    }
  }
  return(urb_table)
}


# apply the functions
plotpath<-wd
shapefileN3 <- readOGR(dsn=file.path(shapefiles_dir,"NUTS3"), layer="NUTS_Level_3_(January_2018)_Boundaries")
urbanN3 <-whatsurban.N(shapefileN3,plotpath)
remove(shapefileN3)
write.csv(urbanN3,"urbanN3.csv", row.names = FALSE)

shapefileN2 <- readOGR(dsn=file.path(shapefiles_dir,"NUTS2"), layer="NUTS_Level_2_(January_2018)_Boundaries")
urbanN2 <-whatsurban.N(shapefileN2,plotpath)
remove(shapefileN2)
write.csv(urbanN2,"urbanN2.csv", row.names = FALSE)

shapefileN1 <- readOGR(dsn=file.path(shapefiles_dir,"NUTS1"), layer="NUTS_Level_1_(January_2018)_Boundaries")
urbanN1 <-whatsurban.N(shapefileN1,plotpath)
remove(shapefileN1)
write.csv(urbanN1,"urbanN1.csv", row.names = FALSE)


shapefileLAD<- readOGR(dsn=file.path(shapefiles_dir,"LAD"), layer="Local_Authority_Districts__December_2019__Boundaries_UK_BFE")
urbanLAD<-whatsurban.LAD(shapefileLAD,plotpath)
remove(shapefileLAD)  
write.csv(urbanLAD,"urbanLAD.csv", row.names = FALSE)






