################################################################################
#### Import Functions and Set WD ###############################################

ukssp_dir<-here::here() # finds the fhe directory where the .rproj file is located
source(file.path(ukssp_dir, 'EnvironSetup.R'))
remove(furthermetrics_dir,population_dir,shapefiles_dir)

source(file.path(ukssp_dir, 'TrendQuantification.R'))

wd<-file.path(capitals_dir,"Manufactured")
setwd(wd)


################################################################################
#### Projections ###############################################################

MBase<-data.frame(read.csv("M Baseline.csv",header=TRUE,sep=',',stringsAsFactors = FALSE))

#### First Indicator ###########################################################

# 1. Baseline
b<-MBase %>% dplyr::select(GFCF.area.2017..m.EUR.km2.)

# 2. Make projections with constant regional variation

#  Define Central Projection Trend (from Stakeholder/Expert Process)
P_M<-c(
  2.29,	3.35,	   7,	   15,	  50, 87.85,	100.35,125.35,125.35, #ssp1
  2.29,	2.29,	2.29,	 2.60,	3.35,	 4.52,	5.35,	 5.35,	5.35, #ssp2
  2.29,	2.29,	1.10,	 0.35,	0.35,	 0.35,	0.35,	 0.35,	0.35, #ssp3
  2.29,	4.35,	  10,	   15,	6.19,	 4.52,	3.35,	 2.60,	2.60, #ssp4
  2.29,	6.19,	  15,	   50,100.35,150.35,175.35,187.85,200.35) #ssp5

# median calculated over NUTS2 areas

median<-2.29
pmin<-0
pmax<-500

p_FC<-projection(b,median,P_M,pmin,pmax)

# 3. Make regional adjustments (from Stakeholder/Expert Process)

p_FC_unadjusted<-p_FC

# SSP1: 50% convergence to the 50th percentile
p_FC<-converge(p_FC,"SSP1",0.5,0.5,pmin,pmax)

# SSP2: No adjustment

# SSP3: 90% convergence to the 30th percentile
p_FC<-converge(p_FC,"SSP3",0.9,0.3,pmin,pmax)

# SSP4: 20% divergence from the 70th percentile
p_FC<-diverge(p_FC,"SSP4",0.2,0.7,pmin,pmax)

# SSP5: No adjustment

# optional to check reg. adj. (this removes the trend, make sure to switch off)
#p_FC<-remove.trend(p_FC,median,P_M,pmin,pmax)

# 4. Clean up and add variable name
remove(b,p_FC_unadjusted,median,P_M,pmax,pmin)
colnames(p_FC)<-c(paste("FC",colnames(p_FC),sep="_"))

# 5. Standardise
st_p_FC<-standardise("FC",p_FC,0,0.75,1.25,3,10,500)

# 6. Merge into one dataframe and clean up
manufactured<-cbind(MBase[,1:9],p_FC,st_p_FC)
remove(p_FC,st_p_FC)


#### Second Indicator ##########################################################

# 1. Baseline
b<-MBase %>% dplyr::select(Road.Infrastructure.2015)

# 2. Make projections with constant regional variation

#  Define Central Projection Trend (from Stakeholder/Expert Process)
P_M<-c(
  0.51,	0.6,	0.7,	0.7,	0.7,	0.7,	0.7,	0.7,	0.7, #ssp1
  0.51,	0.6,	0.7,	0.8,	0.8,	0.8,	0.83,	0.88,	0.9, #ssp2
  0.51,	0.5,	0.5,	0.5,	0.45,	0.37,	0.25,	0.15,	0.15, #ssp3
  0.51,	0.7,	0.85,	0.83,	0.8,	0.8,	0.8,	0.8,	0.8, #ssp4
  0.51,	0.8,	0.9,	0.97,	1.6,	2.5,	  3,  	3,  	3) #ssp5

# Mean value over LAD areas calculated: 0.5063

mean<-0.51
pmin<-0
pmax<-4

p_RI<-projection(b,mean,P_M,pmin,pmax)

# 3. Make regional adjustments (from Stakeholder/Expert Process)

p_RI_unadjusted<-p_RI

# SSP1: No adjustment

# SSP2: No adjustment

# SSP3: 80% convergence to the 50th percentile
p_RI<-converge(p_RI,"SSP3",0.8,0.5,pmin,pmax)

# SSP4: No adjustment

# SSP5: Rural areas' increase 70% lower than the main trend
as<-data.frame(read.csv(file.path(urbanisation_dir,"Results","urbanLAD.csv"),header=TRUE,sep=',',stringsAsFactors = FALSE))
# check if order of as is exactly the same as MBase
identical(as$LAD19CD,MBase$LAD19CD) #good
urb_tld<-0.24
p_RI<-urb.rur.adjust(b,mean,p_RI,"SSP5",P_M,as,urb_tld,0.3,0,pmin,pmax)

# optional to check reg. adj. (this removes the trend, make sure to switch off)
#p_RI<-remove.trend(p_RI,mean,P_M,pmin,pmax)

# 4. Clean up and add variable name
remove(b,p_RI_unadjusted,mean,P_M,pmax,pmin,as,urb_tld)
colnames(p_RI)<-c(paste("RI",colnames(p_RI),sep="_"))

# 5. Standardise
st_p_RI<-standardise("RI",p_RI,0,0.1,0.2,0.3,1,4)

# 6. Merge into one dataframe and clean up
manufactured<-cbind(manufactured,p_RI,st_p_RI)
remove(p_RI,st_p_RI)


#### Create Manufactured Capital ###############################################

MCC<-ccfunction("M","FC","RI",manufactured)
manufactured<-cbind(manufactured,MCC)
remove(MCC,MBase)

write.csv(manufactured,"M Projections.csv", row.names = FALSE)

################################################################################