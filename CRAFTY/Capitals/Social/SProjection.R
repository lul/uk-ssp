################################################################################
#### Import Functions and Set WD ###############################################

ukssp_dir<-here::here() # finds the fhe directory where the .rproj file is located
source(file.path(ukssp_dir, 'EnvironSetup.R'))
remove(furthermetrics_dir,population_dir,shapefiles_dir)

source(file.path(ukssp_dir, 'TrendQuantification.R'))

wd<-file.path(capitals_dir,"Social")
setwd(wd)


################################################################################
#### Projections ###############################################################

SBase<-data.frame(read.csv("S Baseline.csv",header=TRUE,sep=',',stringsAsFactors = FALSE))

#### First Indicator ###########################################################

# 1. Baseline

b<-SBase %>% dplyr::select(S80.S20.Ratio.2011)

# 2. Make projections with constant regional variation

#  Define Central Projection Trend (from Stakeholder/Expert Process)
P_M<-c(
  5.2,	5.2,	4,	4,	2,	1,	1,	1,	1, #ssp1
  5.2,	5.2,	6,	6,	7,	7,	7,	7,	7, #ssp2
  5.2,	5.2,	6,	7,	9,	10,	11,	13,	15,#ssp3
  5.2,	6,  	8,	9,	14,	20,	30,	40,	50,#ssp4
  5.2,	5.2,	4,	4,	4,	4,	4,	4,	4) #ssp5

# S80/S20 Ratio (total UK 2011): 5.188

mean<-5.2
pmin<-1
pmax<-60

p_IE<-projection(b,mean,P_M,pmin,pmax)

# 3. Make regional adjustments (from Stakeholder/Expert Process)

p_IE_unadjusted<-p_IE

# SSP1: 90% convergence to the 50th percentile
p_IE<-converge(p_IE,"SSP1",0.9,0.5,pmin,pmax)

# SSP2: No adjustment

# SSP3: Urban areas' increase should be 20% higher than the main trend
as<-data.frame(read.csv(file.path(urbanisation_dir,"Results","urbanN3.csv"),header=TRUE,sep=',',stringsAsFactors = FALSE))
# check if order of as is exactly the same as MBase
identical(as$nuts318cd,SBase$nuts318cd) #good
urb_tld<-0.19
p_IE<-urb.rur.adjust(b,mean,p_IE,"SSP3",P_M,as,urb_tld,1.2,1,pmin,pmax)

# SSP4: Urban areas' increase should be 30% higher than the main trend
p_IE<-urb.rur.adjust(b,mean,p_IE,"SSP4",P_M,as,urb_tld,1.3,1,pmin,pmax)

# SSP5: No adjustment

# optional to check reg. adj. (this removes the trend, make sure to switch off)
#p_IE<-remove.trend(p_IE,mean,P_M,pmin,pmax)

# 4. Clean up and add variable name
remove(b,p_IE_unadjusted,mean,P_M,pmax,pmin,as,urb_tld)
colnames(p_IE)<-c(paste("IE",colnames(p_IE),sep="_"))

# 5. Standardise
st_p_IE<-rev.standardise("IE",p_IE,60,25,10,5,2,1)

# 6. Merge into one dataframe and clen up
social<-cbind(SBase[,1:9],p_IE,st_p_IE)
remove(p_IE,st_p_IE)


#### Second Indicator ##########################################################


# 1. Baseline

b<-SBase %>% dplyr::select(Neighbours.Help.2015)

# 2. Make projections with constant regional variation

#  Define Central Projection Trend (from Stakeholder/Expert Process)
P_M<-c(
  91,	91,	  95,	  97,	100,100,100,100,100,#ssp1
  91,	91,	  91,	  85,	85,	85,	85,	85,	85, #ssp2
  91,	85,	  80,	  75,	65,	60,	53,	45,	35, #ssp3
  91,	82,	  75,	  65,	55,	40,	35,	30,	25, #ssp4
  91,	88,	  85,	  83,	81,	80,	80,	80,	80) #ssp5

# Neighbours help (2015) average of all usuable responses: 91.1% say yes

mean<-91
pmin<-0
pmax<-100

p_NH<-projection(b,mean,P_M,pmin,pmax)

# 3. Make regional adjustments (from Stakeholder/Expert Process)

p_NH_unadjusted<-p_NH

# SSP1: 90% convergence to the 50th percentile
p_NH<-converge(p_NH,"SSP1",0.9,0.5,pmin,pmax)

# SSP2: No adjustment

# SSP3: No adjustment

# SSP4: 90% convergence from the 50th percentile
p_NH<-converge(p_NH,"SSP4",0.9,0.5,pmin,pmax)

# SSP5: No adjustment

# optional to check reg. adj. (this removes the trend, make sure to switch off)
#p_NH<-remove.trend(p_NH,mean,P_M,pmin,pmax)

# 4. Clean up and add variable name
remove(b,p_NH_unadjusted,mean,P_M,pmax,pmin)
colnames(p_NH)<-c(paste("NH",colnames(p_NH),sep="_"))

# 5. Standardise
st_p_NH<-standardise("NH",p_NH,0,30,50,70,90,100)

# 6. Merge into one dataframe and clean up
social<-cbind(social,p_NH,st_p_NH)
remove(p_NH,st_p_NH)


#### Create Social Capital #####################################################

SCC<-ccfunction("S","IE","NH",social)
social<-cbind(social,SCC)
remove(SCC,SBase)

write.csv(social,"S Projections.csv", row.names = FALSE)

