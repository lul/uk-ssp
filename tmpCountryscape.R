### This is for maps for Countryscape


library(raster)
library(dplyr)
library(ggplot2)
library(rgdal)
library(rgeos)
library(spatstat) # density map
library(here)
library(maps)
library(mapdata)
library(maptools)
library(ggmap)
library(broom)
library(plyr)
library(tidyverse)


################################################################################
#### Set WD ####################################################################


ukssp_dir<-here::here() # finds the fhe directory where the .rproj file is located
source(file.path(ukssp_dir, 'EnvironSetup.R'))
remove(capitals_dir,furthermetrics_dir)

wd<-file.path(ukssp_dir)
setwd(wd)

par(mar = c(0.1,0.1,0.1,0.1)) 

### 1km

lcm<-raster(paste(file.path(urbanisation_dir,"BaselineSurface"),"\\","LCM2015_UK_1km_dominated.tif",sep=""))

r1km<-lcm
r1km[!is.na(lcm)]<-0
plot(r1km)

set.seed(2021)
values(r1km)<-sample(c(0,1), replace=TRUE, size=910000)

#values(r1km)<-rep(c(rep(c(1,0),ncol(r1km)/2),rep(c(0,1),ncol(r1km)/2)),nrow(r1km)/2)

r1km<-mask(r1km,lcm,inverse=FALSE,maskvalue=NA,updatevalue=NA)

plot(r1km,col=c("black","white"),axes=FALSE)


### whole UK

UK<-lcm
UK[!is.na(lcm)]<-1

plot(UK,col=c("black"),axes=FALSE)


### LAD

LAD.shp <- readOGR(dsn = file.path(shapefiles_dir,"LAD"), layer="Local_Authority_Districts__December_2019__Boundaries_UK_BFE")

mapdata <- tidy(LAD.shp, region="LAD19NM") #This might take a few minutes

gg <- ggplot() + geom_polygon(data = mapdata, aes(x = long, y = lat, group = group), color = "white", size = 0.001)
gg <- gg + coord_fixed(1) #This gives the map a 1:1 aspect ratio to prevent the map from appearing squashed
gg <- gg + theme_minimal()
gg <- gg + theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank())
gg <- gg + theme(axis.title.x=element_blank(), axis.text.x = element_blank(), axis.ticks.x = element_blank())
gg <- gg + theme(axis.title.y=element_blank(), axis.text.y = element_blank(), axis.ticks.y = element_blank())
gg <- gg + theme(strip.background = element_blank())
print(gg)

remove(gg,LAD.shp,mapdata)

### NUTS 3

N3.shp <- readOGR(dsn = file.path(shapefiles_dir,"NUTS3"), layer="NUTS_Level_3_(January_2018)_Boundaries")

mapdata <- tidy(N3.shp, region="nuts318nm") #This might take a few minutes

gg <- ggplot() + geom_polygon(data = mapdata, aes(x = long, y = lat, group = group), color = "white", size = 0.001)
gg <- gg + coord_fixed(1) #This gives the map a 1:1 aspect ratio to prevent the map from appearing squashed
gg <- gg + theme_minimal()
gg <- gg + theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank())
gg <- gg + theme(axis.title.x=element_blank(), axis.text.x = element_blank(), axis.ticks.x = element_blank())
gg <- gg + theme(axis.title.y=element_blank(), axis.text.y = element_blank(), axis.ticks.y = element_blank())
gg <- gg + theme(strip.background = element_blank())
print(gg)

remove(gg,N3.shp,mapdata)


### NUTS 2

N2.shp <- readOGR(dsn = file.path(shapefiles_dir,"NUTS2"), layer="NUTS_Level_2_(January_2018)_Boundaries")

mapdata <- tidy(N2.shp, region="nuts218nm") #This might take a few minutes

gg <- ggplot() + geom_polygon(data = mapdata, aes(x = long, y = lat, group = group), color = "white", size = 0.001)
gg <- gg + coord_fixed(1) #This gives the map a 1:1 aspect ratio to prevent the map from appearing squashed
gg <- gg + theme_minimal()
gg <- gg + theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank())
gg <- gg + theme(axis.title.x=element_blank(), axis.text.x = element_blank(), axis.ticks.x = element_blank())
gg <- gg + theme(axis.title.y=element_blank(), axis.text.y = element_blank(), axis.ticks.y = element_blank())
gg <- gg + theme(strip.background = element_blank())
print(gg)

remove(gg,N2.shp,mapdata)


### NUTS 1

N1.shp <- readOGR(dsn = file.path(shapefiles_dir,"NUTS1"), layer="NUTS_Level_1_(January_2018)_Boundaries")

mapdata <- tidy(N1.shp, region="nuts118nm") #This might take a few minutes

gg <- ggplot() + geom_polygon(data = mapdata, aes(x = long, y = lat, group = group), color = "white", size = 0.001)
gg <- gg + coord_fixed(1) #This gives the map a 1:1 aspect ratio to prevent the map from appearing squashed
gg <- gg + theme_minimal()
gg <- gg + theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank())
gg <- gg + theme(axis.title.x=element_blank(), axis.text.x = element_blank(), axis.ticks.x = element_blank())
gg <- gg + theme(axis.title.y=element_blank(), axis.text.y = element_blank(), axis.ticks.y = element_blank())
gg <- gg + theme(strip.background = element_blank())
print(gg)

remove(gg,N1.shp,mapdata)









